#!/bin/bash

echo "=======>>>>>>>Checking minimum requirements for Lition TrustNode..."

ncpu=$(nproc --all)
echo "=======>>>>>>>Your system has $ncpu cores available!"
if [ $ncpu -gt 7 ]; then
    :
else
    echo "!!!!!!!!!The minimum requirement is 8 cores. Please upgrade your system!"
    exit
fi


nram=$(awk '/MemAvailable/ { printf "%.0f\n", $2/1000000 }' /proc/meminfo)
echo "=======>>>>>>>Your system has $nram GB RAM available!"
if [ $nram -ge 7 ]; then
    :
else
    echo "!!!!!!!!!The minimum requirement is 8 GB RAM. Please upgrade your system!"
    exit
fi

echo "=======>>>>>>>Your system meets the minimum requirements for CPU and RAM! Please monitor your free disk space on a monthly basis!"

echo "=======>>>>>>>Updating apt-get packages..."
apt-get -y -qq update

if hash git 2>/dev/null; then
    echo "=======>>>>>>> git is installed!"
else
    echo "=======>>>>>>>Installing git..."
    apt-get -y -qq install git
fi

if hash docker 2>/dev/null; then
    echo "=======>>>>>>>docker is installed!"
else
    echo "=======>>>>>>>Installing docker..."
    curl -sSL https://get.docker.com/ | sh
fi

if [ -d ~/lition-maker ]; then
  echo "=======>>>>>>>Updating lition-maker..."
  cd ~/lition-maker
  git pull 
else
    echo "=======>>>>>>>Cloning lition-maker..." 
    git clone https://gitlab.com/lition/lition-maker.git ~/lition-maker
fi

if hash htop 2>/dev/null; then
    echo "htop is installed! [Optional]"
else
    echo "Installing htop... [Optional]"
    apt-get -y -qq install htop
fi
